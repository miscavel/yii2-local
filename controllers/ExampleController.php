<?php

namespace app\controllers;

use yii\web\Controller; 

class ExampleController extends Controller 
{ 
   public $defaultAction = 'index';

   public function actions()
   {
      return
      [
         'greeting' => 'app\components\GreetingAction',
      ];
   }

   public function actionIndex() 
   { 
      $message = 'index action of the ExampleController'; 
      return $this->render('index',
      [ 
         'message' => $message 
      ]); 
   }

   public function actionHelloWorld()
   {
      return 'Hello World!';
   }

   public function actionOpenGoogle()
   {
      return $this->redirect('http://google.com');
   }

   public function actionTestParams($first, $second)
   {
      return "$first $second";
   }
}

?>